#!/usr/bin/env python
import argparse
from pathlib import Path
import os
import multiprocessing as mp
from warnings import warn

import torch
import torch.optim
import torch.utils.data

import pytorch_lightning as pl
from pytorch_lightning.loggers import TensorBoardLogger
from pytorch_lightning.callbacks import ModelCheckpoint

import utils
import utils.color
import utils.bayer
from model.model import DeviceIndependentModel, DeviceDependentModel
from model.data import TrainBothData, TestA7R3Data, TestD7000Data

os.environ["LRU_CACHE_SIZE"] = "16"
utils.env.load()


parser = argparse.ArgumentParser(description="Inverse Opertion Experiments",
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("mode", choices=["train", "test"], help="train or test")
parser.add_argument("arch", choices=["without_ddnet", "with_ddnet"], help="Model architecture")
parser.add_argument("-O", "--save-root", metavar="PATH", default="results",           help="parent directory to save results")  # noqa: E501
parser.add_argument("--cache-root",      metavar="PATH", default="data/cache",        help="path to cache the splitted dataset")  # noqa: E501
parser.add_argument("--dataroot-rbr",    metavar="PATH", default="data/realblurraw",  help="path to the RealBlur RAW dataset")  # noqa: E501
parser.add_argument("--dataroot-raise",  metavar="PATH", default="data/raise",        help="path to the RAISE dataset")         # noqa: E501
parser.add_argument("--datalist-rbr-train",         metavar="PATH", default="data/realblurraw.train.csv")  # noqa: E501
parser.add_argument("--datalist-rbr-val",           metavar="PATH", default="data/realblurraw.val.csv")    # noqa: E501
parser.add_argument("--datalist-rbr-test",          metavar="PATH", default="data/realblurraw.test.csv")   # noqa: E501
parser.add_argument("--datalist-raise-d7000-train", metavar="PATH", default="data/raise_d7000.train.csv")  # noqa: E501
parser.add_argument("--datalist-raise-d7000-val",   metavar="PATH", default="data/raise_d7000.val.csv")    # noqa: E501
parser.add_argument("--datalist-raise-d7000-test",  metavar="PATH", default="data/raise_d7000.test.csv")   # noqa: E501
parser.add_argument("--crop-size",              metavar="N", default=512, type=int, help="crop size of the input image")            # noqa: E501
parser.add_argument("-j", "--data-workers",     metavar="N", default=4,   type=int, help="number of data loading workers per GPU")  # noqa: E501
parser.add_argument("--max-epochs", "--epochs", metavar="N", default=300, type=int, help="number of total epochs to run")  # noqa: E501
parser.add_argument("-b", "--batch-size",       metavar="N", default=4,   type=int, help="batch size")                     # noqa: E501
parser.add_argument("--lr",           metavar="LR", default=1e-4, type=float, help="learning rate")
parser.add_argument("--lr-step",      nargs="+",    default=20,   type=int,   help="scheduler step")
parser.add_argument("--lr-gamma",     metavar="G",  default=0.9,  type=float, help="scheduler gamma")
parser.add_argument("--momentum",     metavar="M",  default=0.9,  type=float, help="momentum")
parser.add_argument("--weight-decay", metavar="W",  default=1e-8, type=float, help="weight decay")
parser.add_argument("--seed", default=None, type=int, help="random seed for initializing training")
parser.add_argument("--resume", metavar="NAME", default=None, help="checkpoint to resume if exists")            # noqa: E501
parser.add_argument("--load",   metavar="NAME", default=None, help="checkpoint to load weight only if exists")  # noqa: E501
parser.add_argument("--ckpt-epoch", metavar="N", default=50, type=int, help="save checkpoint every N epochs")                # noqa: E501
parser.add_argument("--ckpt-best",  metavar="N", default=3,  type=int, help="save checkpoints for the best N performances")  # noqa: E501
parser.add_argument("--profiler", choices=["simple", "advanced", "pytorch"], default=None, help="profiler to use")  # noqa: E501
parser.add_argument("--save-image", action="store_true", default=False, help="save image when validating or testing")  # noqa: E501


def main():
    args = parser.parse_args()

    if args.data_workers < 0:
        args.data_workers = mp.cpu_count() // (2 * torch.cuda.device_count())

    args.gpus = torch.cuda.device_count()
    args.deterministic = args.seed is not None

    if args.seed is not None:
        pl.seed_everything(args.seed)
        warn("You have chosen to seed training. "
             "This will turn on the CUDNN deterministic setting, which can slow down your training considerably! "
             "You may see unexpected behavior when restarting from checkpoints.")

    args.save_dir = Path(args.save_root, args.arch).as_posix()
    args.ckpt_postfix = "checkpoints"
    args.logs_postfix = "logs"
    args.ckpt_dir = f"{args.save_dir}/{args.ckpt_postfix}"
    args.logs_dir = f"{args.save_dir}/{args.logs_postfix}/{args.mode}"

    if args.resume is not None:
        args.resume = utils.path.purify(args.resume)
        if not Path(args.resume).exists():
            raise FileNotFoundError(f"{args.resume=} does not exist")
    if args.load is not None:
        args.load = utils.path.purify(args.load)
        if not Path(args.load).exists():
            raise FileNotFoundError(f"{args.load=} does not exist")

    Path(args.ckpt_dir).mkdir(parents=True, exist_ok=True)
    Path(args.logs_dir).mkdir(parents=True, exist_ok=True)

    print(args)

    match args.mode:
        case "train": train(args)
        case "test": test(args)
        case _: raise ValueError(f"unknown mode: {args.mode}")


def train(args):
    match args.arch:
        case "without_ddnet": model = DeviceIndependentModel(args)
        case "with_ddnet":    model = DeviceDependentModel(args)
        case _: raise ValueError(f"unknown arch: {args.arch}")

    data = TrainBothData(args)

    logger = TensorBoardLogger(save_dir=args.save_dir, name=args.logs_postfix, version=args.mode)

    callbacks = [
        ModelCheckpoint(
            dirpath=args.ckpt_dir, monitor="val/psnr", mode="max", auto_insert_metric_name=False,
            save_top_k=args.ckpt_best, save_last=True,
            filename="{val/psnr:.2f}-{epoch:03d}"),
        ModelCheckpoint(
            dirpath=args.ckpt_dir, save_top_k=-1, every_n_epochs=args.ckpt_epoch, auto_insert_metric_name=False,
            filename="{epoch:03d}"),
    ]

    trainer: pl.Trainer = pl.Trainer.from_argparse_args(
        args, logger=logger, callbacks=callbacks, profiler=args.profiler)

    results = trainer.fit(model, datamodule=data, ckpt_path=args.resume)
    utils.saveyaml(results, f"{args.logs_dir}/results.yaml")
    trainer.save_checkpoint(f"{args.ckpt_dir}/final.ckpt")


def test(args):
    if not args.resume:
        raise ValueError("resume is required for testing")

    match args.arch:
        case "without_ddnet": model = DeviceIndependentModel(args)
        case "with_ddnet":    model = DeviceDependentModel(args)
        case _: raise ValueError(f"unknown arch: {args.arch}")

    logger = TensorBoardLogger(save_dir=args.save_dir, name=args.logs_postfix, version=args.mode)

    trainer: pl.Trainer = pl.Trainer.from_argparse_args(
        args, logger=logger, profiler=args.profiler)

    results = trainer.test(model, datamodule=TestA7R3Data(args))
    utils.saveyaml(results, f"{args.logs_dir}/results_a7r3.yaml")
    results = trainer.test(model, datamodule=TestD7000Data(args))
    utils.saveyaml(results, f"{args.logs_dir}/results_d7000.yaml")


if __name__ == "__main__":
    main()
