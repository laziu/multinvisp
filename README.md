# Multi-Camera Inverse Image Signal Processing

> **Abstract:** _Inverse problem of image signal processing can be solved by deep learning, but it is difficult to stabilize performance with
> handling multiple cameras. This paper aim to solve this problem by passing appropriate parameters to the network inspired from the
> real image processing pipeline._

## Installation

The model is built in PyTorch and tested on Ubuntu 20.04 environment (Python 3.10, Pytorch 1.11)

`docker-compose.yml` is used to build the model and run the server.

- Run Demo

```sh
# in docker container
$ python experiments_invops.py train network_only a7r3
$ python experiments_invops.py test network_only --resume results/network_only_a7r3/checkpoints/final.ckpt
$ python experiments_invops.py train network_only d7000
$ python experiments_invops.py test network_only --resume results/network_only_d7000/checkpoints/final.ckpt
$ python experiments_invops.py train network_with_invops a7r3
$ python experiments_invops.py test network_with_invops --resume results/network_with_invops_a7r3/checkpoints/final.ckpt
$ python experiments_invops.py train network_with_invops d7000
$ python experiments_invops.py test network_with_invops --resume results/network_with_invops_d7000/checkpoints/final.ckpt
$ python experiments_depnet.py train without_ddnet
$ python experiments_depnet.py test without_ddnet --resume results/without_ddnet/checkpoints/final.ckpt
$ python experiments_depnet.py train with_ddnet
$ python experiments_depnet.py test with_ddnet --resume results/with_ddnet/checkpoints/final.ckpt
```

## Dataset

For Nikon D7000 camera, the dataset is provided by [RAISE dataset](http://loki.disi.unitn.it/RAISE/download.html).

For Sony A7R3 camera, [Custom RealBlur RAW dataset](https://postechackr-my.sharepoint.com/:u:/g/personal/geonukim_postech_ac_kr/ESMLA3BWYXhJj-c1BRGd6OEB8PMCxiKMKOkm8O-2RdFPaw?e=8gG74V) is provided.
