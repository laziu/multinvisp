from argparse import Namespace
from pathlib import Path
import itertools

import torch
import torch.nn as nn
import torch.nn.functional as F
import pytorch_lightning as pl

import utils
import utils.color
import utils.bayer

from . import network


SRGB_D50_TO_XYZ = torch.tensor([[0.4124564, 0.3575761, 0.1804375],
                                [0.2126729, 0.7151522, 0.0721750],
                                [0.0193339, 0.1191920, 0.9503041]], dtype=torch.float32)
SRGB_D65_TO_XYZ = torch.tensor([[0.4360747, 0.3850649, 0.1430804],
                                [0.2225045, 0.7168786, 0.0606169],
                                [0.0139322, 0.0971045, 0.7141733]], dtype=torch.float32)
XYZ_TO_SRGB_D50 = SRGB_D50_TO_XYZ.inverse()
XYZ_TO_SRGB_D65 = SRGB_D65_TO_XYZ.inverse()
SRGB_D50_TO_D65 = SRGB_D50_TO_XYZ @ XYZ_TO_SRGB_D65
SRGB_D65_TO_D50 = SRGB_D65_TO_XYZ @ XYZ_TO_SRGB_D50


class BaseModel(pl.LightningModule):
    def __init__(self, args: Namespace):
        super().__init__()
        self.save_hyperparameters(args)

        self.whitebalance = utils.color.WhiteBalance()
        self.demosaic = utils.bayer.Demosaic5x5()
        self.mosaic = utils.bayer.Mosaic()
        self.colormatrix = utils.color.ColorMatrix()
        self.linear2srgb = utils.color.Linear2SRGB()
        self.srgb2linear = utils.color.SRGB2Linear()

        self.register_buffer("SRGB_D65_TO_D50", SRGB_D65_TO_D50.unsqueeze(0))
        self.register_buffer("XYZ_TO_SRGB_D65", XYZ_TO_SRGB_D65.unsqueeze(0))

        if self.hparams.save_image:
            if self.hparams.mode == "train":
                Path(f"{self.hparams.logs_dir}/val").mkdir(parents=True, exist_ok=True)
            elif self.hparams.mode == "test":
                Path(f"{self.hparams.logs_dir}/test").mkdir(parents=True, exist_ok=True)

    def get_optimizers_and_schedulers(self, params):
        optimizer = torch.optim.Adam(
            params, lr=self.hparams.lr, weight_decay=self.hparams.weight_decay)
        scheduler = torch.optim.lr_scheduler.StepLR(
            optimizer, self.hparams.lr_step, gamma=self.hparams.lr_gamma)
        return {
            "optimizer": optimizer,
            "lr_scheduler": scheduler,
        }

    def destruct_batch(self, batch: dict):
        raw:  torch.Tensor = batch["raw"]  # B, 1, H, W
        rgb:  torch.Tensor = batch["rgb"]  # B, 3, H, W
        mask: torch.Tensor = batch["bayer_mask"].unsqueeze(1).to(torch.int64)    # B, 1, H, W
        wb:   torch.Tensor = batch["white_balance"].unsqueeze(-1).unsqueeze(-1)  # B, 3, 1, 1
        cmat: torch.Tensor = self.XYZ_TO_SRGB_D65 @ batch["color_matrix"]  # B, 3, 3
        camera_names: list[str] = batch["camera_id"]
        image_names:  list[str] = batch["name"]

        cmat = cmat / torch.mean(cmat, dim=-1, keepdim=True)

        H, W = rgb.shape[-2:]
        raw = raw[..., :2 * (H // 2), :2 * (W // 2)]
        rgb = rgb[..., :2 * (H // 2), :2 * (W // 2)]
        mask = mask[..., :2 * (H // 2), :2 * (W // 2)]

        return raw, rgb, mask, wb, cmat, camera_names, image_names

    def convert_to_camera_ids(self, camera_names: list[str]):
        return [
            0 if "SONY" in camera_name else
            1 if "D7000" in camera_name else -1
            for camera_name in camera_names
        ]

    def inverse_mapping(self, rgb: torch.Tensor, mask: torch.Tensor,
                        wb: torch.Tensor, cmat: torch.Tensor, camera_ids: list[int]):
        raise NotImplementedError

    def compute_loss(self, x: torch.Tensor, y: torch.Tensor) -> torch.Tensor:
        loss_con = F.l1_loss(x, y)
        loss_fft = F.l1_loss(torch.fft.fft2(x),
                             torch.fft.fft2(y))
        return loss_con + 0.1 * loss_fft

    def compute_metrics(self, x: torch.Tensor, y: torch.Tensor):
        x = x.clip(0, 1)
        y = y.clip(0, 1)
        return {
            "loss":  self.compute_loss(x, y),
            "psnr":  utils.metrics.psnr(x, y, data_range=1.),
            "ssim":  utils.metrics.ssim(x, y, data_range=1.),
            "mssim": utils.metrics.mssim(x, y, data_range=1.),
        }

    def training_step(self, batch: dict, batch_idx: int):
        raw, rgb, mask, wb, cmat, camera_names, image_names = self.destruct_batch(batch)
        camera_ids = self.convert_to_camera_ids(camera_names)
        raw_est = self.inverse_mapping(rgb, mask, wb, cmat, camera_ids)

        loss = self.compute_loss(raw_est, raw)

        self.log_dict({
            "train/loss": loss
        }, prog_bar=True)

        self.log_dict({
            "step": self.current_epoch,
            "train/epoch/loss": loss,
        }, on_step=False, on_epoch=True)

        return loss

    def crop(self, x: torch.Tensor) -> torch.Tensor:
        return x[..., 2:-2, 2:-2]

    def validation_step(self, batch: dict, batch_idx: int):
        raw, rgb, mask, wb, cmat, camera_names, image_names = self.destruct_batch(batch)
        camera_ids = self.convert_to_camera_ids(camera_names)
        raw_est = self.inverse_mapping(rgb, mask, wb, cmat, camera_ids)

        metrics = self.compute_metrics(self.crop(raw), self.crop(raw_est))

        self.log_dict({
            "step": self.current_epoch,
            "val/loss":  metrics["loss"],
            "val/psnr":  metrics["psnr"],
            "val/ssim":  metrics["ssim"],
            "val/mssim": metrics["mssim"],
        }, on_step=False, on_epoch=True)

        if self.hparams.save_image:
            if batch_idx == 0:
                gain = 8.0
                plot = utils.imgrid([gain * raw, gain * raw_est])
                utils.imsave(plot, f"{self.hparams.logs_dir}/val/{self.current_epoch}.png")

    def test_step(self, batch: dict, batch_idx: int):
        raw, rgb, mask, wb, cmat, camera_names, image_names = self.destruct_batch(batch)
        camera_ids = self.convert_to_camera_ids(camera_names)
        raw_est = self.inverse_mapping(rgb, mask, wb, cmat, camera_ids)

        metrics = self.compute_metrics(self.crop(raw), self.crop(raw_est))

        self.log_dict({
            "step": self.current_epoch,
            "test/loss":  metrics["loss"],
            "test/psnr":  metrics["psnr"],
            "test/ssim":  metrics["ssim"],
            "test/mssim": metrics["mssim"],
        }, on_step=False, on_epoch=True)

        if self.hparams.save_image:
            for i in range(len(image_names)):
                name = f"{camera_ids[i]}_{image_names[i]}".replace("/", "_")
                utils.savetiff(raw[i], f"{self.hparams.logs_dir}/test/{name}.tiff")


class NetworkOnlyModel(BaseModel):
    def __init__(self, args: Namespace):
        super().__init__(args)

        self.net = network.BasicPipeline(3, 3, 24, n_dab=5)

    def configure_optimizers(self):
        return self.get_optimizers_and_schedulers(self.net.parameters())

    def inverse_mapping(self, rgb: torch.Tensor, mask: torch.Tensor,
                        wb: torch.Tensor, cmat: torch.Tensor, camera_ids: list[int]):
        raw: torch.Tensor = rgb
        raw = self.net(raw)
        raw = self.mosaic(raw, mask)
        return raw


class NetworkWithInvOpsModel(BaseModel):
    def __init__(self, args: Namespace):
        super().__init__(args)

        self.net = network.BasicPipeline(3, 3, 24, n_dab=5)

    def configure_optimizers(self):
        return self.get_optimizers_and_schedulers(self.net.parameters())

    def inverse_mapping(self, rgb: torch.Tensor, mask: torch.Tensor,
                        wb: torch.Tensor, cmat: torch.Tensor, camera_ids: list[int]):
        raw: torch.Tensor = rgb
        raw = self.srgb2linear(raw)
        raw = self.net(raw)
        raw = self.colormatrix(raw, cmat.inverse())
        raw = self.mosaic(raw, mask)
        raw = self.whitebalance(raw, 1 / wb, mask)
        return raw


class DeviceIndependentModel(BaseModel):
    def __init__(self, args: Namespace):
        super().__init__(args)

        self.indnet = network.BasicPipeline(3, 3, 24, n_dab=4)
        self.depnet = network.BasicPipeline(3, 3, 24, n_dab=1)

    def configure_optimizers(self):
        return self.get_optimizers_and_schedulers(
            itertools.chain(self.indnet.parameters(), self.depnet.parameters()))

    def inverse_mapping(self, rgb: torch.Tensor, mask: torch.Tensor,
                        wb: torch.Tensor, cmat: torch.Tensor, camera_ids: list[int]):
        raw: torch.Tensor = rgb
        raw = self.srgb2linear(raw)
        raw = self.indnet(raw)
        raw = self.depnet(raw)
        raw = self.colormatrix(raw, cmat.inverse())
        raw = self.mosaic(raw, mask)
        raw = self.whitebalance(raw, 1 / wb, mask)
        return raw


class DeviceDependentModel(BaseModel):
    def __init__(self, args: Namespace):
        super().__init__(args)

        self.indnet = network.BasicPipeline(3, 3, 24, n_dab=4)
        self.depnet = nn.ModuleList([
            network.BasicPipeline(3, 3, 24, n_dab=1)
            for _ in range(2)
        ])

    def configure_optimizers(self):
        return self.get_optimizers_and_schedulers(
            itertools.chain(self.indnet.parameters(),
                            self.depnet[0].parameters(),
                            self.depnet[1].parameters()))

    def inverse_mapping(self, rgb: torch.Tensor, mask: torch.Tensor,
                        wb: torch.Tensor, cmat: torch.Tensor, camera_ids: list[int]):
        raw: torch.Tensor = rgb
        raw = self.srgb2linear(raw)
        raw = self.indnet(raw)

        raw_ = [None] * len(camera_ids)
        for i, camera_id in enumerate(camera_ids):
            assert 0 <= camera_id < 2
            raw_[i] = self.depnet[camera_id](raw[i].unsqueeze(0))
        raw = torch.cat(raw_, dim=0)

        raw = self.colormatrix(raw, cmat.inverse())
        raw = self.mosaic(raw, mask)
        raw = self.whitebalance(raw, 1 / wb, mask)
        return raw
