from argparse import Namespace

import torch.utils.data
import pytorch_lightning as pl

import data


class BaseData(pl.LightningDataModule):
    def __init__(self, args: Namespace):
        super().__init__()
        self.save_hyperparameters(args)

        self.root_rbr = self.hparams.cache_root + "/realblur_raw"
        self.root_raise = self.hparams.cache_root + "/raise_raw"

    def prepare_data(self):
        self.train_dataset(check=True)
        self.val_dataset(check=True)
        self.test_dataset(check=True)

    def train_dataset(self, check=False): ...
    def val_dataset(self, check=False): ...
    def test_dataset(self, check=False): ...

    def train_dataset_args(self, check=False):
        return {
            "skip_check": not check,
            "crop_config": data.RandomCropConfig(self.hparams.crop_size + 5),
            "unify_bayer": True,
        }

    def val_dataset_args(self, check=False):
        return {
            "skip_check": not check,
            "crop_config": data.CenterCropConfig(self.hparams.crop_size + 5),
            "unify_bayer": True,
        }

    def test_dataset_args(self, check=False):
        return {
            "skip_check": not check,
            "unify_bayer": True,
        }

    def train_dataset_a7r3(self, check=False):
        return data.CachedRealBlurRaw(
            self.hparams.dataroot_rbr, self.hparams.datalist_rbr_train,
            cache_dir=self.root_rbr, **self.train_dataset_args(check=check))

    def train_dataset_d7000(self, check=False):
        return data.CachedRAISERaw(
            self.hparams.dataroot_raise, self.hparams.datalist_raise_d7000_train,
            cache_dir=self.root_raise, **self.train_dataset_args(check=check))

    def val_dataset_a7r3(self, check=False):
        return data.CachedRealBlurRaw(
            self.hparams.dataroot_rbr, self.hparams.datalist_rbr_val,
            cache_dir=self.root_rbr, **self.val_dataset_args(check=check))

    def val_dataset_d7000(self, check=False):
        return data.CachedRAISERaw(
            self.hparams.dataroot_raise, self.hparams.datalist_raise_d7000_val,
            cache_dir=self.root_raise, **self.val_dataset_args(check=check))

    def test_dataset_a7r3(self, check=False):
        return data.CachedRealBlurRaw(
            self.hparams.dataroot_rbr, self.hparams.datalist_rbr_test,
            cache_dir=self.root_rbr, **self.test_dataset_args(check=check))

    def test_dataset_d7000(self, check=False):
        return data.CachedRAISERaw(
            self.hparams.dataroot_raise, self.hparams.datalist_raise_d7000_test,
            cache_dir=self.root_raise, **self.test_dataset_args(check=check))

    def train_dataloader(self):
        return torch.utils.data.DataLoader(
            self.train_dataset(),
            batch_size=self.hparams.batch_size, shuffle=True, drop_last=True,
            num_workers=self.hparams.data_workers, pin_memory=False, persistent_workers=True)

    def val_dataloader(self):
        return torch.utils.data.DataLoader(
            self.val_dataset(),
            batch_size=self.hparams.batch_size, shuffle=False, drop_last=True,
            num_workers=self.hparams.data_workers, pin_memory=False, persistent_workers=True)

    def test_dataloader(self):
        return torch.utils.data.DataLoader(
            self.test_dataset(),
            batch_size=1, shuffle=False, drop_last=True,
            num_workers=self.hparams.data_workers)


class TrainA7R3Data(BaseData):
    def train_dataset(self, check=False):
        return self.train_dataset_a7r3(check=check)

    def val_dataset(self, check=False):
        return self.val_dataset_a7r3(check=check)


class TrainD7000Data(BaseData):
    def train_dataset(self, check=False):
        return self.train_dataset_d7000(check=check)

    def val_dataset(self, check=False):
        return self.val_dataset_d7000(check=check)


class TrainBothData(BaseData):
    def train_dataset(self, check=False):
        return data.Concatenate(
            self.train_dataset_a7r3(check=check),
            self.train_dataset_d7000(check=check),
        )

    def val_dataset(self, check=False):
        return data.Concatenate(
            self.val_dataset_a7r3(check=check),
            self.val_dataset_d7000(check=check),
        )


class TestA7R3Data(BaseData):
    def test_dataset(self, check=False):
        return self.test_dataset_a7r3(check=check)


class TestD7000Data(BaseData):
    def test_dataset(self, check=False):
        return self.test_dataset_d7000(check=check)
