import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision.models
import kornia.losses
from torch import Tensor

from torchmetrics.functional import peak_signal_noise_ratio as psnr
from torchmetrics.functional import structural_similarity_index_measure as ssim
from torchmetrics.functional import multiscale_structural_similarity_index_measure as mssim


class VGGLoss(nn.Module):
    def __init__(self,
                 feature_layers: list[int] = [0, 1, 2, 3],
                 style_layers: list[int] = [],
                 resize=True):
        """Compute a loss based on VGG Perceptual loss.

        Args:
            feature_layers: VGG16 layers to use in feature perceptual loss.
            style_layers: VGG16 layers to use in style perceptual loss.
            resize: resize into fixed size before calculate loss if True.
            device: target device.
        """
        super().__init__()
        self.feature_layers = feature_layers
        self.style_layers = style_layers
        self.resize = resize

        blocks = [
            torchvision.models.vgg16(pretrained=True).features[:4].eval(),
            torchvision.models.vgg16(pretrained=True).features[4:9].eval(),
            torchvision.models.vgg16(pretrained=True).features[9:16].eval(),
            torchvision.models.vgg16(pretrained=True).features[16:23].eval(),
        ]
        for bl in blocks:
            for p in bl:
                p.requires_grad = False
        self.blocks = torch.nn.ModuleList(blocks)

        self.mean: Tensor
        self.register_buffer("mean", torch.tensor([0.485, 0.456, 0.406]).view(1, 3, 1, 1))
        self.std: Tensor
        self.register_buffer("std",  torch.tensor([0.229, 0.224, 0.225]).view(1, 3, 1, 1))

    def __call__(self, x: Tensor, y: Tensor) -> Tensor:
        """ Compute the VGG perceptual loss. """
        return super().__call__(x, y)

    def forward(self, x: Tensor, y: Tensor):
        x = (x - self.mean) / self.std
        y = (y - self.mean) / self.std
        if self.resize:
            x = F.interpolate(x, mode="bilinear", size=(224, 224), align_corners=False)
            y = F.interpolate(y, mode="bilinear", size=(224, 224), align_corners=False)

        loss = 0.0
        for i, block in enumerate(self.blocks):
            x: Tensor = block(x)
            y: Tensor = block(y)
            if i in self.feature_layers:
                loss += F.l1_loss(x, y)
            if i in self.style_layers:
                act_x = x.reshape(x.shape[0], x.shape[1], -1)
                act_y = y.reshape(y.shape[0], y.shape[1], -1)
                gram_x = act_x @ act_x.permute(0, 2, 1)
                gram_y = act_y @ act_y.permute(0, 2, 1)
                loss += F.l1_loss(gram_x, gram_y)

        return loss


__all__ = [
    "psnr",
    "ssim",
    "mssim",
    "VGGLoss",
]
