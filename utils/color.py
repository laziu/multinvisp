from typing import Literal

import torch
import torch.nn as nn
import kornia.color
import kornia.enhance
import numpy as np


class WhiteBalance(nn.Module):
    def __init__(self, mode: Literal["channel", "bayer"] = "bayer"):
        """ Apply white balance to the images.

        Args:
            mode: Mode of the white balance. Available options:
                - `channel`: Apply for multichannel images, such as RGB image.
                - `bayer`: Apply for Bayer images. `pattern` must be provided.
        """
        super().__init__()
        self.mode = mode

    def __call__(self, x: torch.Tensor, wb: torch.Tensor, bayer_mask: torch.Tensor = None) -> torch.Tensor:
        """ Mosaicing RGB images to Bayer pattern.

        Args:
            x: Input RGB image.
            wb: White balance factors for each instance.
            bayer_mask: Bayer pattern for each instance if mode is `bayer`.
        """
        return super().__call__(x, wb, bayer_mask)

    def forward(self, x: torch.Tensor, wb: torch.Tensor, bayer_mask: torch.Tensor = None) -> torch.Tensor:
        B, C, H, W = x.shape
        C = 3 if self.mode == "bayer" else C

        wb = wb.to(x.dtype).view(B, C, 1, 1)

        if self.mode == "bayer":
            x_rgb = torch.cat([x, x, x], dim=1) * wb

            assert bayer_mask.shape[-2:] == (H, W), "bayer_mask shape must be (B, 1, H, W)"

            x = torch.gather(x_rgb, dim=1, index=bayer_mask)
        else:
            x = x * wb

        return x


class ColorMatrix(nn.Module):
    def __init__(self, matrix: torch.Tensor = None):
        """ Apply color matrix to the images.

        Args:
            matrix: Color matrix, set to `None` to use individual matrix for each instance.
        """
        super().__init__()
        self.matrix = torch.as_tensor(matrix) if matrix else None

    def _apply(self, fn):
        if self.matrix is not None:
            self.matrix = fn(self.matrix)
        return super()._apply(fn)

    def __call__(self, x: torch.Tensor, matrix: torch.Tensor = None) -> torch.Tensor:
        """ Apply color matrix to the images.

        Args:
            x: Input image.
            matrix: Color matrix for each instance which overrides the default matrix.
        """
        return super().__call__(x, matrix)

    def forward(self, x: torch.Tensor, matrix: torch.Tensor = None) -> torch.Tensor:
        B, C, H, W = x.shape
        x = x.view(B, C, H * W).swapaxes(-2, -1)  # B, H*W, C

        matrix = matrix.view(B, C, C) if matrix is not None else self.matrix.repeat(B, 1, 1)

        assert C == matrix.size(-1), "Color matrix can only be applied to RGB images"
        x = torch.bmm(x, matrix.transpose(-1, -2))  # B, H*W, C

        x = x.swapaxes(-1, -2).view(B, C, H, W)
        return x


class SRGB2Linear(nn.Module):
    def __init__(self) -> None:
        """ Convert sRGB to linear RGB. """
        super().__init__()
        self.module = kornia.color.RgbToLinearRgb()

    def __call__(self, x: torch.Tensor) -> torch.Tensor:
        """ Convert sRGB to linear RGB.

        Args:
            x: Input image.
        """
        return super().__call__(x)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        return self.module(x)


class Linear2SRGB(nn.Module):
    def __init__(self):
        """ Convert linear RGB to sRGB. """
        super().__init__()
        self.module = kornia.color.LinearRgbToRgb()

    def __call__(self, x: torch.Tensor) -> torch.Tensor:
        """ Convert linear RGB to sRGB.

        Args:
            x: Input image.
        """
        return super().__call__(x)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        return self.module(x)


__all__ = [
    "WhiteBalance",
    "ColorMatrix",
    "SRGB2Linear",
    "Linear2SRGB",
]
